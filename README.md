# LyMaster

> LyMaster 是一款使用 [LyApi][1] 作为基础开发的接口管理系统。方便开发者快速创建接口。

## 项目介绍

LyMaster 是一款基于 [LyApi][1] 开发框架的在线接口管理系统，开发者可随时随地的创建并编辑接口代码。内置：接口管理、用户管理、数据库管理、在线代码编辑器、在线接口生成器，以及高自由度的插件管理系统！LyMaster 本体代码也运行在 [LyApi][1] 之上，只要熟悉 LyApi 即可自由的对 LyMaster 进行二次开发。

## 在线文档

[LyMaster 在线文档](http://lyapi-development.gitee.io/lymaster/#/)

## 图片展示

![欢迎页面.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=dc90K0bn2uc-c_WVG6wNEwKA7pJheTqqmS8sMPONPJSavvuMYu4SyK9ijVy1unUEAcWn38hTE-xjqEWk41BNe_SIRW1nxdM7fc4H-P4Hj1ypY2mL1RV8J78C4L2mP7_9V8_a-1s_9jrbdA&file_name=/welcome.png)

![接口管理.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=c200ZTR7pW68bP4Hph8i5MtzhsjJCDsSz4-RCTcJvUoNgCYwsM6WZdmepNJTCvmtd1TOMubmFyXOVzorZ2dnu8TxCC9WHYPLw8n55qXP4NkfpZDRt-4eHM6oZslUTrtkaQsCHFGzOlcuaRj3lms&file_name=/api-manager.png)

![在线代码编辑器.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=076cyZVB1-VDg48iOkaDqV7ekOOoLZwHMCPrERHpalrKJi3JLVjQy3qC-7HeZmesx8mIdkVtDi8gg2Jz0Ll8Nu4NlMn_oQ2AIYtnfvVutqvQFwLKhSl1nExuHxXHg5ObNJ6rxZyFYnk&file_name=/coder.png)

![代码生成器.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=ba54C3BwgwrQ56oSmsD32X5aNYGijD5Dg6D3SkIez2-zIchk7SMlunp5pqB9gYV_rJ_3OKfYYGoA96J9PdSAuV1ilMr795TqpsaW_XLDWVrlsBuLLAphqgIY8SSm_AXZ37FF4JfQPCI&file_name=/maker.png)

![插件页面.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=80e82c3IdLHwSEsBgzYSclAazOc85IKDUNUlu3wFEdiM7-PPhcmwP6CVC8SgEmEtYqO5XOSYqYHg3Q_8Siv9GsFMw4SQElbrTFstTvfhfvW7QP51X9gv9k1r7djxUpBJNoUsK1hUM6-5&file_name=/plugin.png)

![在线命令行.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=83eds93TTSlEcZj2YiwIVpTJOs6ZCODY5Um8Bktm-HAbtbY1ZJ2CMfdSDaCx8nTOOij-RZSBqhvoAuhP3qoXCpVRMhAWjKYVD0I_HfJ_4zR1XtC5doaRUY8iDteSYANcO09ONRoa7PE&file_name=/cmder.jpg)

![message.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=7ab1Ievnq5zkAUdS9f4W3QFQCttL-EN8s4OSr7bj-eef2Hnr6xDUFlUQBnlE_DOZjfLb4tdaQLQwBueQpwKdiWgdwKO0USiu-z04KcfdmQSMMPgdWnoznpsFrtFxeJZh8Ag7mu5Ix4yDYA&file_name=/message.jpg)

## 独特功能

* 代码编辑器：可在线编辑所有项目文件，自带大量代码补全功能（支持自定义）
* 代码生成器：安装自定义组件后，无代码即可生成接口程序。
* 消息中心：系统支持站内消息发送，包括但不限于：插件操作请求，管理员间传信，用户反馈系统。
* 在线命令行：独创命令行系统，支持插件绑定、创建命令。

## 插件系统

> 框架内置插件：Exaster

默认插件可允许用户使用 MarkDown 编辑站点主页！同时自带用户反馈系统，可使得用户能快速向接口系统管理员反馈遇到的问题与BUG！

插件系统支持开发者对系统内的大部分功能进行操作：接口注册、页面注册、控制台生成、命令注册、主页接管、数据库处理、消息发送等功能。

## 演示站点

> 我们提供了当前测试版的演示站供用户查看后台内容

* 站点路径：http://master.wwsg18.com/admin/
* 测试账号：test
* 测试密码：lymastertest



## 官方工具包（开发中）

「MTool」官方工具包正在开发中！工具包使用GO语言开发，暂不开放源码。

工具包将包含以下功能：

1. LyMaster 与 MTool 间通信。
2. 定时任务设置功能。
3. 代码自动同步功能。
4. 与系统相关的功能操作。



## 特别鸣谢

#### 相关项目

1. [LyApi 开发框架](https://gitee.com/mrxzx/LyApi)
2. [LayUI 前端框架](https://gitee.com/sentsin/layui)
3. [LayUI Mini 后台模板](https://gitee.com/zhongshaofa/layuimini) 


## 信息获取

> 本系统的信息会在以下途径公布

1. 官方 Q Q 群: 769094015
2. 作者个人博客: https://blog.wwsg18.com/
3. 官方应用论坛: http://bbs.wwsg18.com/t/projects/

## 开发日志

* 2020-03-21: 完成初步框架配置。
* 2020-03-22: 实现应用安装页面开发。
* 2020-03-29: 完成后台登录，配置等功能。
* 2020-04-02: 完成在线编辑器程序。
* 2020-04-09: 完成接口管理，用户管理功能。
* 2020-04-10: 修复系统部分运行BUG。
* 2020-04-12: 完善部分辅助工具系统。
* 2020-04-15: 修复部分安装BUG，同时更新权限系统。
* 2020-04-17: 更新多数据库连接操作。
* 2020-04-23: 修复部分数据渲染BUG。
* 2020-04-25: 完成自定义模板部分功能。
* 2020-04-26: 完成 Maker 组件回收站。
* 2020-04-27: 完成 Maker 一键创建接口。
* 2020-05-13: 优化部分安装功能。
* 2020-05-14: 优化部分页面展示数据。
* 2020-05-22: 优化权限系统分类。
* 2020-07-05: 完成相关配套插件。
* 2020-08-25: 更新SQL控制台功能。
* 2020-09-24: 完成error页面美化。

[1]: http://lyapi.wwsg18.com/