-- LyApi - Admin 数据库安装程序 --

-- 用户信息库 --
CREATE TABLE IF NOT EXISTS `$prefix$users`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '用户ID码',
    `username` VARCHAR(255) NOT NULL COMMENT '用户名 (登录使用)',
    `nickname` VARCHAR(255) NOT NULL COMMENT '用户昵称 (显示使用)',
    `password` VARCHAR(255) NOT NULL COMMENT '用户登录密码',
    `level` INT UNSIGNED DEFAULT 1 COMMENT '用户等级',
    `secret` VARCHAR(30) NOT NULL COMMENT '用户加密码',
    `email` VARCHAR(80) COMMENT '用户邮箱',
    `usable` INT UNSIGNED DEFAULT 1 COMMENT '用户状况',
    `createtime` INT(11) NOT NULL COMMENT '创建时间'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 系统设置库 --
CREATE TABLE IF NOT EXISTS `$prefix$setting`(
    `comment` VARCHAR(255) PRIMARY KEY COMMENT '设置名称',
    `context` VARCHAR(255) NULL COMMENT '设置内容'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 用户等级配置 --
CREATE TABLE IF NOT EXISTS `$prefix$user_level`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '等级ID',
    `title` VARCHAR(30) NOT NULL COMMENT '等级展示标题',
    `douser` INT(1) UNSIGNED DEFAULT 0 COMMENT '是否允许操作用户',
    `doapi` INT(1) UNSIGNED DEFAULT 1 COMMENT '是否允许操作接口',
    `dodb` INT(1) UNSIGNED DEFAULT 0 COMMENT '是否允许操作数据库',
    `doplugin` INT(1) UNSIGNED DEFAULT 1 COMMENT '是否允许操作插件',
    `dosetting` INT(1) UNSIGNED DEFAULT 1 COMMENT '是否允许更新配置',
    `docoder` INT(1) UNSIGNED DEFAULT 0 COMMENT '拥有Coder最高权限'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 系统缓存库 --
CREATE TABLE IF NOT EXISTS `$prefix$caches`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '缓存ID',
    `cachekey` VARCHAR(255) NOT NULL COMMENT '缓存Key',
    `cacheval` VARCHAR(255) NOT NULL COMMENT '缓存Value',
    `cachedue` VARCHAR(255) NOT NULL COMMENT '缓存过期时间'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 系统接口库 --
CREATE TABLE IF NOT EXISTS `$prefix$apis`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '接口ID',
    `apiname` VARCHAR(255) NOT NULL COMMENT '接口名称',
    `apipath` VARCHAR(255) NOT NULL COMMENT '接口路径',
    `apiabout` VARCHAR(255) NOT NULL COMMENT '接口介绍',
    `apisecure` INT(11) NOT NULL COMMENT '安全级别',
    `apitype` INT UNSIGNED DEFAULT 1 COMMENT '接口类型',
    `apistatus` INT UNSIGNED DEFAULT 1 COMMENT '接口状态',
    `finaledit` INT(11) NOT NULL COMMENT '最后更新'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 接口模板库 --
CREATE TABLE IF NOT EXISTS `$prefix$api_templates`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '模板ID',
    `name` VARCHAR(40) NOT NULL COMMENT '模板名称',
    `type` INT(1) UNSIGNED DEFAULT 1 COMMENT '模板类型',
    `savetype` INT(1) UNSIGNED DEFAULT 1 COMMENT '储存方式',
    `path` VARCHAR(255) NOT NULL COMMENT '模板地址'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据库连接库 --
CREATE TABLE IF NOT EXISTS `$prefix$database`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '数据库ID',
    `name` VARCHAR(50) NOT NULL COMMENT '数据库名称',
    `type` ENUM('mysql') DEFAULT 'mysql' COMMENT '数据库类型',
    `host` VARCHAR(100) NOT NULL COMMENT '数据库地址',
    `port` INT(50) UNSIGNED DEFAULT 3306 COMMENT '储存方式',
    `username` VARCHAR(255) NOT NULL COMMENT '登录账号',
    `password` VARCHAR(255) NOT NULL COMMENT '登录密码',
    `charset` VARCHAR(50) DEFAULT 'utf8' COMMENT '数据库编码',
    `status` INT(1) UNSIGNED DEFAULT 1 COMMENT '数据库状态'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 程序日志库 --
CREATE TABLE IF NOT EXISTS `$prefix$logs`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '日志ID',
    `type` INT(1) UNSIGNED DEFAULT 1 COMMENT '日志类型',
    `from` VARCHAR(100) NOT NULL COMMENT '操作对象',
    `doip` VARCHAR(50) NOT NULL COMMENT '来源IP',
    `info` VARCHAR(255) NOT NULL COMMENT '具体信息',
    `time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
    `remark` VARCHAR(255) DEFAULT '' COMMENT '备注信息'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 插件数据表 --
CREATE TABLE IF NOT EXISTS `$prefix$plugin`(
    `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '插件ID',
    `sign` VARCHAR(50) NOT NULL COMMENT '插件标识',
    `name` VARCHAR(100) NOT NULL COMMENT '插件名称',
    `version` VARCHAR(20) NOT NULL COMMENT '插件版本',
    `status` INT(1) DEFAULT 0 COMMENT '插件状态',
    `database` VARCHAR(255) NOT NULL COMMENT '绑定数据库',
    `path` VARCHAR(255) NOT NULL COMMENT '插件路径',
    `config` VARCHAR(255) NOT NULL COMMENT '插件配置',
    `time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 插入系统自带数据
INSERT INTO `$prefix$users` (`username`,`nickname`,`password`,`level`,`secret`,`email`,`createtime`)
                            VALUES
                            ('admin','Administrator','$password$',1,'$secret$','admin@$domain$','$time$');

INSERT INTO `$prefix$user_level` (`title`,`douser`,`doapi`,`dodb`,`doplugin`,`dosetting`,`docoder`) VALUES ('高级管理员',1,1,1,1,1,1);

INSERT INTO `$prefix$setting` (`comment`,`context`) VALUES ('site_title','LyApi Admin System');
INSERT INTO `$prefix$setting` (`comment`,`context`) VALUES ('site_notice','本系统使用 LyApi - Admin 搭建。您可以在管理页面修改本公告哦！');
INSERT INTO `$prefix$setting` (`comment`,`context`) VALUES ('site_copyright','© admin.lyapi.wwsg18.com');
INSERT INTO `$prefix$setting` (`comment`,`context`) VALUES ('site_cache_time','5');
INSERT INTO `$prefix$setting` (`comment`,`context`) VALUES ('site_show_notice','0');

INSERT INTO `$prefix$api_templates` (`name`,`type`,`savetype`,`path`) VALUES ('演示模板',1,1,'api/demo.php');
INSERT INTO `$prefix$api_templates` (`name`,`type`,`savetype`,`path`) VALUES ('空白模板',1,1,'api/empty.php');
INSERT INTO `$prefix$api_templates` (`name`,`type`,`savetype`,`path`) VALUES ('演示模板',2,1,'view/demo.php');
INSERT INTO `$prefix$api_templates` (`name`,`type`,`savetype`,`path`) VALUES ('空白模板',2,1,'view/empty.php');