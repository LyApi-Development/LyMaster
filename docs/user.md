# 多用户管理

> LyMaster 支持创建多个用户账号，并进行简单分级（不完善）

系统安装时，默认用户（即第一个账号）为最高管理员（ID: 0）

0 号 ID 账号禁止被删除，同时本账号拥有所有权限！

用户可在后台的个人设置中更新自己的账号信息与密码。

## 密码找回

> 原版系统暂无密码找回机制，但后期可能会在插件中实现！

### 暴力找回

最原始的密码更新方法：修改后台数据库！

```php
// 密码加密算法
$result = md5(md5($password) . $secret);
// 先对密码进行 md5, 再与加密码合并进行一次 md5
```

!> 请不要泄露用户加密码（secret）给任何人！！

### 密码缓存

系统提供了密码缓存系统（安装2小时内可查）

位于路径：**domain/admin/security?program=retrieve**

!> 查询登录后，请立刻更新密码并删除备份信息！！！

## 权限分配

> 用户权限组分配的大概规划！

由于本系统当前的权限分配机制较为简单，所以分配方式也不多。

目前总共有 **6** 份权限，分别为：

- douser - 用户操作权限
- doapi - 接口操作权限
- dodb - 数据库操作权限
- doplugin - 插件操作权限
- dosetting - 配置操作权限
- docoder - 编辑器操作权限

每一份权限决定了这一名用户是否能进行相关的操作！

#### 危险权限

用户操作、配置操作、数据库操作 属于危险权限，请勿随意分配！

#### 推荐分配

- 高级管理员 - 全部开启
- 中级管理员 - 接口、数据库、插件、配置、编辑器
- 低级管理员 - **仅**接口

> 权限系统并不完善，后期会更新更加细致的分类！

## 友情提示

由于权限系统并不完善，建议您仅为您**信任的人**开通账号！