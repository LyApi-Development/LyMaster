# 接口系统

> 框架基于 LyApi 开发，接口生成也同时继承了 LyApi 的方式！

## LyApi 接口框架

[LyApi](https://gitee.com/mrxzx/LyApi) 是一款轻量级的 PHP 接口开发框架，可快速开发出易维护、高性能的 API 接口。内置缓存、日志、数据库操作、国际化等功能。

LyMaster 的接口开发方式与 LyApi 完全一致，更多开发方法请前往 [LyApi - Docs](https://mrxzx.gitee.io/lyapi-docs/#/) 查看！

## 推荐开发工具

如果您是一位开发者，我们推荐您使用 VSCode 进行接口开发！

**SFTP + VSCode** 可实现实时代码上传功能！详情见：[使用 SFTP 完成代码同步](https://blog.wwsg18.com/index.php/archives/18/)

后期我们也会推出 **vsc2ms** 插件，实现更加强大的接口代码同步！

#### 其他

还有一个选择：**LyMaster - Coder** (LyMaster 在线编辑器)

用这玩意改改代码还是可以，写代码的话就算了...