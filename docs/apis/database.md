# 数据库连接

> LyMaster 数据库连接方法会比 LyApi 更加简单！

在 LyApi 中，我们需要对配置文件进行修改，以达成对数据库的连接（Medoo）

但在 LyMaster 中，仅需在后台完成连接保存，即可快速连接（Medoo）

![db.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=a604i-fuNA69mSbuM9N-NCrrymoDHSFjC3NTvUr0_MyB8LYPoDQaIuAzUjZ6KOkYH-9nJVRCJtupLKbeEdG-xrHLdZMqDCfl51UTfuikmHWPAwT4DLnSgRC0Ms7gGqtJgZ57-nE0dLNR_04&file_name=/todb.jpg)

当您的连接被成功创建后，便可直接在代码中连接它！

```php
$db_connect = DI::Medoo("lymaster");    // 使用数据库名取得连接
$db_connect = DI::Medoo(1);             //或者使用数据库ID号也行

// 养成一个好习惯：连接后先查是否成功
if(!$db_connect){
    throw new ServerException("数据库连接错误",0);
}

```

最后附上 Medoo 官方文档地址：https://medoo.in/doc
