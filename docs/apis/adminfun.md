# 后台系统函数

## 用户信息验证

> 用于验证登录信息，用户权限等信息！

目标命名空间地址：**APP\program\admin\Verify**

```php
use APP\program\admin\Verify;

Verify::isLogin(); // 判断用户是否登录

Verify::encryptPwd($password, $secret); // 密码验证函数（普通开发者无用处）

Verify::authCheck($authority); // 权限检查：docoder,douser,doapi,dodb,dosetting

```

!> 在需要相关用户操作时，请务必检查用户是否登录！否则会出现安全问题！！

## 用户信息获取

用户信息使用 **Session** 储存，直接使用 **\$\_SESSION** 获取即可！

```php
$_SESSION["nickname"];  // 用户昵称
$_SESSION["username"];  // 用户名称
$_SESSION["userid"];    // 用户ID号
```

当 **Session** 过期后，用户会退出登录。

## 配置信息操作

> 部分配置操作功能函数！

目标命名空间地址：**APP\program\admin\Setting**

```php
use APP\program\admin\Setting;

$status = Setting::isInstall(); // 检查系统是否安装（毫无意义）

$db_connect = Setting::dbConnect(); // 取得系统数据库连接（危险，谨慎操作）

$config_info = Setting::getSetting($comment); // 取得某系统信息（数据库中）

$cutedb = Setting::localDB($dbname); // 打开一个 CuteDB连接（官方操作）
```
