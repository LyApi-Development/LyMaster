# 信息发送程序

> LyMaster 内置了消息中心，每名用户都有各自的消息储存机制。

信息发送程序允许接口程序对某位后台用户发送信息。

做个简单的比喻：框架内置的 **Exaster** 插件在被开启后会对最高管理员发起主页接管申请！

这一申请使用对的是消息发送机制，消息中心允许程序自定义全内容（HTML）

## 使用场景

#### 用户反馈

Exaster 插件提供了用户反馈功能，用户输入的消息会被自动发送到最高管理员的消息中心。

#### 插件通知

插件可以对管理员发起一些信息通知，如：版本更新、系统到期、安全漏洞 等。

#### 接口信息

当某接口被调用，可向管理员发送信息。（在接口内部实现发送程序）

## 发送方法

```php
use APP\program\admin\user\Notification; // 命名空间引入

// 内容为HTML代码（请完善页面样式）
$context = file_get_contents("message.html");

// 来源需要易于分辨，如：插件名称，操作名称
$from = "文档演示系统";

// 类型：info,warning,error,success
$type = "info";

// 预览内容为纯文本
$preview = "您收到了新的演示信息 [message]";

// 目标用户ID，最高管理员默认为 0
$to = 0;

Notification::sendMessage($context,$from,$type,$preview,$to);
```