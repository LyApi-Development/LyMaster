# 编辑器全览系统

> 全览系统可以让开发者打开本站点内的所有文件夹并进行编辑。

![我编辑我自己.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=f9d3n0-eiPtAzotTZZkz1JD8ovJ8xQ_BWK4LeaSnDYADxNnB9ZWDBegWxv1vsnyhfhWHaCvqe2-r_p5wiO0w9dq_PO0in-78_Emd3MmNM5PiO3nOSlsQmPfIbBuL0UV71vUVoBfrL-PxlVdv9piI&file_name=/edself.jpg)

QWQ: 我编辑我自己！！

## 开关全览

高级配置: 位于 **/config/admin/summit.json** (本文件默认被全览隐藏)

请手动编辑本文件以完成配置！

```json
{
    "system": {
        "root_path": "/www/wwwroot/api"
    },
    "coder": {
        "open_overview": true,
        "overview_filter": [
            "summit.json",
            ".vscode"
        ]
    }
}
```

数据项介绍:
* system.root_path: 当前站点系统根路径，用于部分数据检查。
* coder.open_overview: 是否打开 OVERVIEW 功能，默认为打开。
* coder.overview_filter: 同 coder.json 的过滤项。

!> 这里 summit.json 会从 OVERVIEW 过滤自身（建议保持，开放会有危险！！）

## 注意事项

有几点需要注意一下:
* OVERVIEW 只能 **打开/编辑** 有**权限**的文件！
* 切勿随意编辑框架本体文件，可能导致系统全面崩溃！
* 不使用建议关闭 OVERVIEW 功能，有安全风险！