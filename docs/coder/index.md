# 代码编辑器 - Coder

> 一款在线代码编辑器 [FOR LyMaster]

## 功能介绍

#### 接口代码编辑

可快速打开系统创建的接口程序代码，并进行编辑。

#### 生成器缓存编辑

可对 Maker 在线代码生成器的函数代码进行浏览与更新，同时支持编辑更新缓存代码。

#### 系统全文件编辑

开启 OVERVIEW 后可对本系统路径下的所有文件进行更新 (我编辑我自己QWQ)

#### 自定义配置系统

可根据需求自定义编辑器配置，包括但不限于: 
* font-size: 文字大小
* default-language: 默认语言补全包
* default-theme: 默认编辑器皮肤包
* custom-block: 自定义代码提示块
* overview_filter: 全览过滤文件列表
* overview_often: 全览常用文件列表