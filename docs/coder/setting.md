# 配置本工具

> 工欲善其事，必先利其器。先把编辑器配置好吧！

## 配置文件

代码编辑器路径: http://domain/admin/coder

![setting.json](http://pan.wwsg18.com/index.php?user/publicLink&fid=3715R66BrZzNvSF4zVE4ooaA-n_BBZSB0bUBG8wgfrQCBusBOArTXz2VxXC7x3YplNkWkl1kwThQTTvBm8Po9LTA_mXxoU42vj56B3Wr8NJOPE0fr9CNe4qUjmAach_VgO8MRbSkkq6f7Y34oEddrA&file_name=/setting.png)

位于左侧导航栏的**编辑器设置** (或直接打开系统目录: **config/admin/coder.json**)

## 基础配置

### 字体大小配置
**Font-Size**: 默认数值为22，推荐数值在 20 - 24 之间，小于20字体可能会比较小。

### 编辑器公开状态
**Public-View**: 选择编辑器的公开状态，支持三个值: **open,close,readonly**

* **open**: 代码编辑器将对所有人公开！(非常危险，别选嗷嗷！)
* **close**: 代码编辑器仅对已登录并有权限的人公开！(非常安全，建议选这个！)
* **readonly**: 代码编辑器对已登录但没权限的人实现只读！

默认 **close** ,建议保留哦:dog:

### 默认高亮语言

> 项目都是PHP的，当然PHP用的最多啦！[Default: PHP]

**Default-Language** 给几个常用的值，自己选就行了:
* PHP (世界上最好的语言)
* HTML (HTML算语言)
* JavaScript (JS天下第一)
* Json (配置文件必用)
* MarkDown (README专用)

上面这些语言包可能会用得到！

!> 其实保留PHP就行，因为编辑器会根据打开的文件自动切换语言包！

### 默认主题包

> 代码编辑器包含很多主题，可手动切换！

**Default-Theme** 有很多样式: 

* ambiance
* chaos
* chrome
* clouds
* cobalt
* dawn
* dracula
* dreamweaver
* eclipse
* github
* gob
* gruvbox
* kuroir

样式太多了，可以自己去看看: [https://www.bootcdn.cn/ace/](https://www.bootcdn.cn/ace/)

## 自定义代码块

> 本配置可实现自定义代码提示信息。

**Custom-block**默认代码块配置:
```json
{
    "name": "DI Class",
    "value": "DI::",
    "caption": "DI",
    "meta": "lyapi block",
    "type": "local",
    "score": "1000"
}
```

数据项介绍:
* name: 为本代码块取个名字。
* value: 当本代码块被选择后插入到编辑器到值。
* caption: 展示代码块时的标题信息。
* meta: 展示代码块时的介绍信息。
* type: 代码块类型。
* score: 代码块优先级。

## 全览过滤器

**overview_filter** 可以设定被OVERVIEW过滤的文件。

由于 .git 和 .vscode 并无实质性用处，所以将其隐藏。

!> PS: 本隐藏仅会在 全览列表 中隐藏同名文件。

## 全览快开列表

> 全览快开列表可以让编辑器将部分文件放置于左侧菜单栏。

```json
{
    "title": "标题",
    "file": "文件名称",
    "path": "文件路径"
}
```

数据项介绍:
* title: 用于展示在侧边栏。
* file: 本地文件名称。
* path: 文件路径，不包含文件名称。