# 系统配置

> 安装完成后，需要先对平台进行配置！

## 基础配置

![setting.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=c09ef8ksYdN_HUzTlXt1vVIH9nSRfEqvPOANeK_n_HxgvVaijaLM0GP1DAaID3IC6tkt01nZpiXmYj1cDH2czRsM0DtzEJ52g-OOrDN3WGyNtU_-wR3tKPJ3PYwymmBKuwvb26S4HVWXfzK4s_VM&file_name=/bsetting.jpg)

* **网站名称**: 当前系统名称，会在部分页面展示。 
* **网站域名**: 当前系统域名 (系统自动识别，无法修改)
* **缓存时间**: 部分缓存数据的过期时间，建议设置在 **1 - 5** 之间。
* **系统公告**: 公告会在主页展示，可随意修改。
* **版权信息**: 暂无任何用处，可先填写。

## 高级配置

> 点击系统配置下的 高级配置 即可打开配置页面。

```json
{
    "framework-debug": true,
    "cache-system": "file",
    "cache-setting": {
        "file": {
            "rpath": "admin",
            "forbid-clean": ["vstate","maker"]
        },
        "redis": {
            "host": "127.0.0.1",
            "port": 6379,
            "database": 0
        }
    },
    "vstate-setting": {
        "filter": [
            {
                "using-function": "admin",
                "first-path": "root"
            },
            {
                "first-path": "admin"
            },
            {
                "using-function": "favicon.ico",
                "first-path": "root"
            }
        ]
    }
}
```

### framework-debug

是否打开 **LyApi Debug** 调试功能。

开启后，会有明确的错误信息，同时 **LyMaster** 的DEBUG页面将允许被访问。

### cache-system

系统使用的缓存系统，支持: **file** 和 **redis**

默认全部使用 file 缓存，如果开启 redis 后依旧会有部分数据使用 file 缓存。

!> Redis 连接配置会在 cache-setting.redis 下。

### cache-setting

缓存配置，包括 文件缓存 和 Redis缓存 配置。

#### 文件缓存

rpath: 文件缓存根路径 "**data/cache/{rpath}/**"

forbid-clean: 清空缓存时过滤掉的文件夹，默认不会清空 "vstate" 和 "maker"

#### Redis缓存

```json
{
    "host": "127.0.0.1",
    "port": 6379,
    "database": 0
}
```

* host: 数据库连接地址
* port: 数据库连接端口
* database: 使用的数据库(建议单独留一个空数据库)

### vstate-setting

> 本配置项为 访问量统计器 的设置！

**vstate-setting.filter** 用于过滤接口的统计器。

```json
{
    "using-function": "admin",
    "first-path": "root"
},
{
    "first-path": "admin"
}
```

上方的代码会过滤掉所有路径为: "****admin/\*****" 的访问。

```json
{
    "using-function": "favicon.ico",
    "first-path": "root"
}
```

上方的代码则会只过滤掉路径为: "**/favicon.ico**" 的文件访问。