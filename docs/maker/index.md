# 代码生成器 Maker

> 一款方便快捷的代码生成器 \[FOR LyMaster\]

## 功能介绍

* 快速生成代码
* 高自由度组件
* 一键导出接口
* 快速编辑代码

## 详细介绍

Maker 是为不熟悉PHP开发的小白准备的，它可以让你仅完成几步配置就创建出接口（需要组件配套），Maker 的组件系统极其强大，他可以让你做任何事情！同时你可以在代码编辑器：Coder 中预览或修改已创建的 Maker 缓存代码。

## 组件功能

组件是开发者可以自行开发的一套配置！通过组件，可以让生成器完成一切需要的功能！组件需要通过用户对表单的输入，生成对应的目标代码，并将代码提交给 Maker 进行处理。