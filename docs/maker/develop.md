# 组件开发教程

> 组件系统可以让开发者们完成任何需求，并分享给其他开发者！

## 组件示例

> 系统自带 组件DEMO，位于: admin/components/LatCT

```
目录结构: 
LatCT -> custom.php // 操作代码(对组件数据进行处理)
      -> setting.json // 组件配置(让 MAKER 读取组件信息和配置)
```

## 组件配置

以下是 Demo组件的配置文件:
```json
{
    "name": "自定义操作",
    "sign": "UCG",
    "form": [{
        "name": "codes",
        "label": "textarea",
        "placeholder": "自定义PHP接口代码程序...",
        "rows": 16,
        "lay-verify": "required",
        "style": "font-size:20px;"
    },{
        "name": "imports",
        "label": "input",
        "type": "text",
        "autocomplete": "off",
        "placeholder": "需要引入的命名空间（多个使用,隔开）"
    },{
        "name": "about",
        "label": "info",
        "style": "color:red;",
        "value": "提示：本组件为官方提供的DEMO组件，仅用于组件开发学习！"
    }],
    "console": "prompt[请输入生成的函数名:funcname]",
    "filter": "values.filter",
    "manager": "custom.manager"
}
```
本组件的配置文件比较多，这里依次介绍！

### 名称和标识

name 和 sign 分别代表了组件名称和标识符。

名称可以随便定义，注意一定要让其他开发者一眼就能看出用途！

标识则建议设置一个独一无二的值，长一点也没关系，比如: "XZ.DEMO" // 开发者.名称

### 表单组件

在 Maker 中选择一个组件后，都会跳出一堆表单，这些表单则是在这里配置。

目前有四个不同的表单组件供开发者选择: **input**, **textarea**, **info** 和 **select**

* **input**: 普通输入框组件
* **textarea**: 多行文本框组件
* **info**: 内容(公告)展示组件
* **select**: 下拉框组件

name 数据项为必填，当提交数据被传递到处理器时，需要通过 name 来访问对应的值。

其余的可根据 HTML 标签属性自行增加！

#### info 专属值

info 表单组件有一个专属值为: value，本项定义了要显示的内容。

#### select 专属值

info 表单组件有一个专属值为: child (一个数组)，定义了下拉框中的 options

```json
{
    "name": "dbs",
    "label": "select",
    "info": "这是下拉框的默认值",
    "child": {
        "mysql": "MySQL",
        "sqlse": "SQL Server",
        "sqlite": "SQLite"
    }
}
```

当 **child** 为一个字符串时，则会前往目标函数动态取得下拉框数据。

```json
{
    "name": "dbs",
    "label": "select",
    "info": "这是下拉框的默认值",
    "child": "custom.dbs"
}
```

上方 Demo 会访问 custom.php 下的 dbs 函数，并取得 child 值。


### 控制台页面

> 当程序自带的表单系统无法满足需求，你可以自行创建一个配置页面。

console 项填写弹出的控制台页面，输入同目录的 **HTML文件** 名称即可。

Demo 中则是使用了一个 **prompt** 框，就是一个简单的输入框。

#### 自定义控制台

> 自定义控制台的数据同样需要传递给处理器。

只需将表单结果通过 **POST** 方式发送到: /admin/api/maker/managers 下即可。格式:
```
{
    comp: 组件SIGN标识符
    data: 表单数据信息
}
```


#### prompt 输入框

    prompt[显示的内容:变量名称]

变量名称会同以上的表单组件共同传递到最终的处理器。

## 过滤器与处理器

!> 过滤器(filter) 暂时无用处，但必须保留。

### 处理器 - 数据处理程序

系统会根据 manager 项找到处理器函数。

**custom.manager** 则代表 custom目录下的manager函数。

```php
// 结果最终处理程序
public function manager($options)
{
    // options 参数将接收到所有表单填入的数据，可以自行分配。
    return [
        "template" => $options['codes'],
        "imports" => explode(",",$options['imports']),
        "funcname" => $options['funcname']
    ];
}
```
最终的返回值需要三个数据，**template**, **imports** 和 **funcname**

**Template** 为最终的模板代码(不需要携带函数声明，仅函数内的方法)

**Imports** 需要引入的代码包(比如: app\DI)

**Funcname** 最终生成的函数名字！可要求用户自定义，也可自动生成

## 最终结果

> 经过处理器后的程序则会进入系统的代码组合区。

代码组合器会将结果进行融合，生成一个完整的函数程序:

![结果.jpg](http://pan.wwsg18.com/index.php?user/publicLink&fid=d2e21ZsOtIQ5B6_Xb2Xr12Eihlg_gtL36_VgvKWfuDXnbdzREsNJWCvrvzsqvqvvqMIN2QH723Rwht6eXxW7tejbmNe4Lcqo31xmit5bOJodx9g94_mPBcnkr8ZNyI4RC7T01_2HOtywWsR0XSWV&file_name=/mcache.jpg)

结果会存放在系统的缓存中，等待生成接口或生成模板的操作。但开发者可以在编辑器中查看或修改缓存内容。