# 基础 命令行工具

## 信息获取

### WHOAMI

> 一个很有意思的指令「WHOAMI」，它可以告诉你你是谁？虽然这听起来很扯淡。

```Cmder Command
whoami -u
```
后方参数为需要取得的信息类型：
* default -  NickName(昵称 - 即显示名称)
* -U - UserName(用户名 - 即登录名称)
* -N - NickName(昵称 - 即登录名称)

### SYSINFO

> 跳出一个系统信息栏，可以看到简单的信息。(真的很简单很简单的信息！)

```Cmder Command
sysinfo
```

* 系统类型：{SYSTEM - VERSION}
* 后台版本：{VERSION} -  LyApi Admin
* 技术支持：LyApi 项目开发组

## 系统缓存

### 清理缓存

> 一个快速清理缓存的命令！！

```Cmder Command
cache clean
```
本命令会清空系统文件缓存（仅文件缓存）

## 命令行操作

### 清空命令行

> 将命令行中的所有命令信息清除！

```Cmder Command
clean [cls]
```

同时支持 clean 和 cls 两个关键词。快捷键: **Ctrl + l**

### 