# Maker 命令行工具

## 引用管理

> 我们可以使用 Cmder 快速更新 Maker 中的引入文件。

### 命令格式

```Cmder Command
maker icd/include [options] [argument]
```

### 添加引用

``` Cmder Command
maker icd add foo\bar
```

本操作将添加一个引入到 Maker 的 Include 列表中！

或者使用 foo.bar 代替 foo\bar 也同样支持（这样会更加优雅）

### 删除引用

``` Cmder Command
maker icd del foo\bar
```

本操作将从 Maker 的 Include 列表中删除一个引入！

或者使用 foo.bar 代替 foo\bar 也同样支持（这样会更加优雅）

### 清空引用

``` Cmder Command
maker icd clean
```

本操作将清空所有 Maker 的 Include 数据！

### 引入列表

``` Cmder Command
maker icd list
```

本操作将显示所有当前已存在的 Include 数据！

## 函数管理

> 我们可以使用 Cmder 操作 Maker 中的函数信息。

### 函数列表

``` Cmder Command
maker func list
```

本操作将显示所有当前已存在的函数数据！