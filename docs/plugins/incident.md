# 事件处理函数

> 事件函数即在某个事件发生时会被调用的函数！

**LyMaster** 的插件并不需要手动调用，而是在一些特定的时候被主动调用。

## 构造函数

函数名: **__construct**

在框架处理过程中，自始至终使用的对象都是同一个，所以构造函数可以完成一些初始化操作！

## 安装时函数

函数名: **onInstall**

当插件被安装时所调用的函数，您可以完成一些初始化操作：数据库创建，配置文件生成 等。

!> 严禁进行任何非法操作，如有发现会永久将插件与开发者列入黑名单！

## 卸载时函数

函数名: **onUninst**

当插件被卸载时调用的函数，您可以完成一些操作：数据库销毁，配置文件删除 等。

## 页面列表函数

函数名：**onLoadPages**

当 LyMaster 管理页面列表被加载时执行！

请将 **页面注册程序** 写入本函数，用于在后台创建管理页！

## 接口注册函数

函数名: **onRegisterApi**

当 LyMaster 正在查找接口时被调用（优先级低于系统接口）

插件注册 **接口 OR 页面** 需要在本函数中操作！

## 命令注册函数

函数名: **onRegisterCmd**

当 LyMaster - Cmder 正在查找命令时被调用（优先级低于系统命令）

插件注册 **命令行** 需要在本函数中操作！

## 主页渲染函数

函数名: **indexPage**

在普通插件中本函数无意义（除非需要申请主页渲染权）

当插件取得主页控制权，本函数将在每次主页访问时调用，框架会将本函数返回的内容返回到页面！