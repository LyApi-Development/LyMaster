# 代码编写

> 上面简单的介绍了插件系统，接下来我们来写代码吧！！

## 插件位置

LyMaster 系统插件位于：**admin/plugin** 下！

!> 根目录下的 **plugin** 为 LyApi 框架 的插件目录，请不要搞错了！

插件文件夹下有一个 **Template** 目录，这就是插件开发的模板代码！

```
目录结构：
- data          // 数据文件
- view          // 视图文件
  - index.html
- options.php   // 插件主程
```

## 插件处理器

> 插件系统提供了 Processor 对象，用于插件与框架系统交互。

```php
$this->processor = new Processor();
```

插件模板已初始化 **processor** 可直接使用！

### 管理页面注册

```php

$options = [
    'title' => "模板演示", // 选项标题显示
    'icon' => 'fa fa-cog', // 选项图标显示 (fontawesome)
    'context' => "<h1> Hello LyMaster </h1>" // 页面内容(建议读取HTML文件)
];

processor->registerSP($this, $options); // 注册后台管理页面
```

注册管理页面的代码建议放在 **onLoadPages** 函数中！

### 接口程序注册

```php

// 接口配置
$options = [
    "type" => 'API',        // API 类型会作为接口数据被返回，VIEW 则是渲染页面
    "path" => "demo/*"     // 使用了通配符 * 访问任何 demo/xxx 都会被分配到此
];

// 回传数据
$backval = []; // 可以将部分数据传入到函数中

processor->registerAPI(function($type, $args, $backval){

    // $type: 访问类型
    // $args: 传递参数
    // $backval: 回传数据

    return "OK"; // 最终输出的数据

}, $options, $backval);
```

上方代码使用了通配符，在 **\$backval["holder"]** 中我们可以取得通配符填写的值！

比如：

```php
processor->registerAPI(function($type, $args, $backval){

    return "hello: " . $backval["holder"][0]; // 访问第一个通配符值

    // 访问: 127.0.0.1/demo/mrxiaozhuox
    // 返回: hello: mrxiaozhuox

}, $options, $backval);
```

本方法支持 LyApi 框架的所有操作！[查看官方文档](https://mrxzx.gitee.io/lyapi-docs/#/README)

### 主页权限申请

```php
// 申请取得主页的渲染控制权，传入当前插件名称
processor->applyIndexPage(self::NAME);
```

最高管理员将收到一条申请信息，当申请被通过后主页则会被接管！

!> 请勿频繁发送请求，否则会被列入危险插件行列！

### 系统命令申请

```php
$trigger = ["demo","hello"]; // 命令触发指令，支持多个（不区分大小写）
processor->registerCommand($trigger, function ($commands, $num) {
    // $commands: 命令后跟随的参数（数组）
    // $num: 参数数量（包括本身）

    return "Hello World"; // 返回的内容（支持HTML标签）
});
```

建议不要在结果添加太多样式！


## 更多函数

插件开发中也可以信息发送系统，详情见：[接口相关 - 信息发送程序](/apis/notification)