# 插件配置

```php
// 插件基础设置

const SIGN = "Template";                                   // 框架标识（文件夹名）
const NAME = '插件开发模板';                                // 插件名称（展示名）
const VERSION = "V1";                                    // 插件当前版本号
const PATH = LyApi . '/admin/plugin/' . self::SIGN;     // 插件根目录路径
const USABLE = true;                                   // 插件是否可用

// 插件数据库列表
public $tables = []; // 数据库列表(预留，后期有用)

public $config = [
    "debug" => true, // 是否开启调试模式（预留）
    "paid" => false // 是否为收费插件（预留）
];

```

!> 插件标识需要独一无二，最好别和其他插件相同！(**标识 = 文件夹名 = 命名空间**)


## 收费插件

如果您希望您的插件开启收费模式，请打开 **$config["paid"]** (暂无用处)

同时您需要自备授权验证程序，并准备好查询接口。后续版本 LyMaster 会自动检查插件的收费情况。