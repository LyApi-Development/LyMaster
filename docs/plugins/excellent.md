# 优秀插件

> 如果您有好的插件，可以修改文档并发起 [Pull Request][pr]

## 开源插件

- [Exaster][exa] - 系统官方扩展插件 @[mr小卓X](https://blog.wwsg18.com)

## 收费插件

[pr]: https://gitee.com/LyApi-Development/LyMaster/pulls
[exa]: https://gitee.com/LyApi-Development/LyMaster/tree/master/admin/plugin/Exaster
