# 项目部署

> 接下来让我们康康项目该如何部署！

## 下载代码

我推荐去 [Gitee][0] 下载代码！那里的版本都是最新的！(也可能会遇到不稳定的版本！)

或者，你可以去官方QQ群看看，那里的版本都是本人精挑细选出来的: **769094015**

请不要从其他途径下载！系统会涉及一些比较危险的数据，如果下载了被**魔改**的版本，不保证数据安全！

## 伪静态配置

> 伪静态真的是我遇到最多的问题！都要崩溃了！！

如果你是 Nginx 服务器，请试试：

```nginx
if (!-f $request_filename){
	set $rule_0 1$rule_0;
}
if ($rule_0 = "1"){
	rewrite ^/(.*)$ /index.php?/$1 last;
}
```

如果是 Apache 服务器，可以用：

```apache
<IfModule mod_rewrite.c>
 RewriteEngine on
 RewriteCond %{REQUEST_FILENAME} !-f
 RewriteRule ^(.*)$ index.php?/$1 [QSA,PT,L]
</IfModule>
```
Apache 框架已经帮你配置好啦！你只需要将工作目录指向 /public 下！

## 工作目录

本项目的工作目录位于 /public 下！如若不指向本路径，则无法正常运行！

当你配置好上一步（伪静态）后，访问路径会由框架程序处理，并自动转到目标页面。

## 目录权限

由于本项目含有**在线代码编辑器**功能，编辑器支持对所有项目内文件进行浏览和修改。

所以，需要给予框架对应的目录(或文件)的操作权限。

#### 必须开放(**777权限**)的文件夹:

* config 目录 (用于自动写入配置文件)
* app 目录 (用于接口文件创建)
* data 目录 (用于数据存储操作)
* admin 目录 (用于系统自身修改操作)

> 一个简单粗暴的方法：直接全上 777 权限！(实不相瞒我就是这么干的QWQ)

## 数据库准备

首先你需要确保你的环境中有 **MySQL** 并且正在运行。

MySQL 是一个数据库，官网: [https://www.mysql.com/](https://www.mysql.com/)（给萌新们）

然后请你在 MySQL 中创建一个数据库！什么名字都行（别问我怎么创，自己去百度！）

PS: 保险一点的话，你还可以创一个专属的用户哦！无论如何安全第一嘛。

到这里数据库准备就完成了，后面的内容是做一些拓展！

### 缓存数据库

本系统支持强大的 **Redis** ！我们会使用分担一部分缓存。(你不装也行，那就全用文件缓存喽！)

系统默认全部缓存功能都由 LyApi 的文件缓存承担，但你也可以配置部分使用 Redis

这里再给萌新们丢个 Redis 的官网: [https://redis.io/](https://redis.io/)

## 系统安装

> 最后一步，也是最简单的一步！

打开你部署好的网站后，它会自动跳转到安装页面，完成操作即可。

操作非常简单，相信萌新们也能看懂...

这里就不给截图了，主要是我太懒了哈哈哈哈！

#### 安装失败的可能:

1. 部署错误会导致安装失败。
2. 数据库连接错误会导致安装失败。
3. 系统不完整会导致安装失败。
4. 必要目录无权限会导致安装失败。

[0]: https://gitee.com/LyApi-Development/LyMaster