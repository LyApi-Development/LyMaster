<?php

// 程序运行出现问题请先运行check.php文件！检测框架是否完整！！！

define('LyApi',dirname(dirname(__FILE__)));
define('LyMaster','LyApi.Admin.System');

require_once LyApi . '/vendor/autoload.php';

use LyApi\tools\Config;
use Plugin\PConfig\PConfig;

session_start();

// 检测后台设置
$debug_config = Config::getConfig('develop','admin','json');
if($debug_config['framework-debug']){
    $whoops = new \Whoops\Run();
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();
}

// 初始化程序配置

$pconfig = new PConfig('OCore');
$pconfig->InitConfig("maps", array(
    "Resource" => LyApi . "/app/view/static/",
    "resource" => LyApi . "/app/view/static/",
));
