<?php

namespace Admin\components\LatCT;

use APP\DI;
use APP\program\admin\Setting;

/**
 * LyApi - Admin System
 * LatCT 程序处理器 (本程序可用于组件开发学习)
 */

class custom
{
    // 结果最终处理程序
    public function manager($options)
    {
        // options 参数将接收到所有表单填入的数据，可以自行分配。
        return [
            "template" => $options['codes'],
            "imports" => explode(",",$options['imports']),
            "funcname" => $options['funcname']
        ];
    }
}