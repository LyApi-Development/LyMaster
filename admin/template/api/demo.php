<?php

namespace __namespace;

use APP\DI;
use LyApi\core\classify\API as ClassifyApi;
use LyApi\core\error\ClientException;
use LyApi\tools\Launch;
use LyApi\core\request\Cookie;
use LyApi\core\request\Request;
use LyApi\tools\Config;
use LyApi\Logger\Logger;
use LyApi\tools\Language;

/**
 * 提供 LyApi 开发框架的原生 Demo 程序 (仅用于学习)
 */

class __class extends ClassifyApi
{

    /**
     * service __class.hello
     * introduce 简单的Hello World
     */
    public function hello()
    {
        return 'Hello LyApi';
    }

    /**
     * service __class.parm
     * introduce 简单的Get参数测试
     */    
    public function parm(){
        return 'Hello ' . Request::Get('Name');
    }

    /**
     * service __class.cache
     * introduce 设置缓存并读取
     */
    public function cache()
    {

        $cache = DI::FileCache('demo');
        //设置缓存
        $cache->set('username', 'mrxzx');
        $cache->set('password', '123456');
        //读取缓存
        $username = $cache->get('username');
        $password = $cache->get('password');
        //清除缓存
        $cache->clean();
        //返回数据
        return array(
            'username' => $username,
            'password' => $password
        );
    }

    /**
     * service __class.language
     * introduce 语言翻译功能测试
     */
    public function language()
    {
        $language = Request::Get('lang');

        return Language::Translation('Hello World', $language);
    }

    /**
     * service __class.cookie
     * introduce 设置cookie并读取
     */
    public function cookie()
    {
        $cookie = new Cookie('/');
        //设置Cookie
        $cookie->Set('username', 'mrxzx');
        $cookie->Set('password', '123456');
        //获取Cookie
        $username = $cookie->Get('username');
        $password = $cookie->Get('password');
        //返回数据
        return array(
            'username' => $username,
            'password' => $password
        );
    }

    /**
     * service __class.register
     * introduce 模拟简单注册
     */
    public function register()
    {
        //获取Get数据
        $username = Request::Get('username');
        $password = Request::Get('password');

        if ($username != '' && $password != '') {
            //使用缓存来做简单的注册
            $cache = DI::FileCache('demo');
            $data = $cache->get('user');
            //判断缓存中是否有用户数据
            if ($data != null) {
                //如果有则加入数据
                $data[$username] = $password;
                $cache->set('user', $data);
            } else {
                //没有则直接存入数据
                $cache->set('user', [$username => $password]);
            }
            return true;
        } else {
            throw new ClientException('参数不完整', 0);
        }
    }

    /**
     * service __class.login
     * introduce 模拟简单登录
     */
    public function login()
    {
        //获取Get数据
        $username = Request::Get('username');
        $password = Request::Get('password');

        if ($username != '' && $password != '') {
            //读取缓存
            $cache = DI::FileCache('demo');
            $data = $cache->get('user');
            
            if (isset($data[$username]) && $data[$username] == $password) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new ClientException('参数不完整', 0);
        }
    }

    /**
     * service __class.logger
     * introduce 日志测试
     */

    public function logger()
    {
        $logger = new Logger();
        $logger->SetLogger([
            'time' => date('Y-m-d'),
            'typs' => 'Test',
            'message' => '这是一条测试日志，通过 __class.logger 函数写入。'
        ]);

        return $logger->GetLastLogger();
    }

    /**
     * service __class.plugin
     * introduce 插件测试
     */
    public function plugin()
    {
        //使用DI下的PluginDyn函数可以动态获取对象
        $demo = DI::PluginDyn('Template', 'Template');
        return $demo->GetPluginName();
    }

    /**
     * service __class.customMsg
     * introduce 自定义数据测试
     */
    public function customMsg()
    {
        return array(
            '#code' => 201,
            '#msg' => 'Hello Lyapi',
            'Title' => 'Lyapi'
        );
    }

    /**
     * service __class.customConfig
     * introduce 自定义配置文件测试
     */
    public function customConfig()
    {
        return Config::getConfig('Test');
    }

    /**
     * service __class.funcData
     * introduce 自定义 Code 和 Message
     */
    public function funcData()
    {

        $this->SetFuncData([
            'code' => 233,
            'msg' => 'Hello __class.funcData'
        ], 'FuncData');


        return 'Hello LyApi';
    }

    /**
     * service __class.hiddenKey
     * introduce 隐藏某个Key的显示
     */
    public function hiddenKey()
    {
        //仅隐藏 hiddenKey
        $this->HiddenKeys('msg', 'hiddenKey');

        //隐藏所有函数的显示（这种方法需要写在 构造函数 或 初始函数 中）
        // $this->HiddenKeys('msg');

        return 'Message is Hidden';
    }

    /**
     * service __class.runApi
     * introduce 内部启动接口代码
     */
    public function runApi()
    {
        // 当前功能自定义数据仅支持 Retunrn 方法
        return Launch::LaunchApi('__namespace', 'customMsg');

        // 如果是FuncData式，则不会处理
        // return Launch::LaunchApi('APP\api\__namespace','funcData');   
    }

    /**
     * service __class.test
     * introduce 自行测试代码
     */
    public function test()
    {
        // Write your code ......
    }
}