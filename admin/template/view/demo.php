<?php

namespace __namespace;

use LyApi\core\classify\VIEW as ClassifyView;

/**
 * 提供简单的视图渲染程序
 */

class __class extends ClassifyView
{
    // 一个简单的页面渲染程序
    public function index()
    {
        return '<h1>当您看到本页面时，您的 Demo 已经成功运行了！</h1>';
    }
}