<?php

namespace Plugin\LyMaster;

use APP\DI;
use APP\program\admin\Cache;
use Exception;
use LyApi\tools\Config;
use Plugin\Core\Core;
use Plugin\LyMaster\tools\Logger as ToolsLogger;

/**
 * Name: LyMaster.LyMaster
 * Author: LyAPI
 * ModifyTime: 2020/03/28
 * Purpose: LyMaster 专属插件
 */

class LyMaster extends Core
{

    //设置插件信息（请严格按照本模板编写代码）
    public function __construct()
    {
        $this->Plugin_Name = 'LyMaster';
        $this->Plugin_Version = 'V0.2Beta';
        $this->Plugin_Author = 'mrxiaozhuox';
        $this->Plugin_About = 'LyApi Admin 专属插件';
        $this->Plugin_Examine = '';

        if (!defined('LyMaster')) {
            throw new Exception('Unknown development environment');
        }
    }

    // 访问量统计程序
    public function visitState($api, $data = 0, $option = [])
    {

        $cache = Cache::cacheObject('vstate', true);
        $ctype = $cache[1];
        $cache = $cache[0];

        if ($ctype == 'file') {
            $date = date('Y-m-d');

            if ($data == 0) {
                // 完成访问操作

                if ($cache->has($api)) {
                    $result = $cache->get($api);
                    if (array_key_exists($date, $result)) {
                        $result = $result[$date];
                    } else {
                        if (count($result) > 7) {
                            for ($i = 8; $i <= count($result); $i++) {
                                unset($result[date('Y-m-d', strtotime('-' . $i . ' days'))]);
                            }
                        }
                        $result[$date] = 0;
                        $cache->set($api, $result);
                        $result = 0;
                    }
                } else {
                    $result = 0;
                    $cache->set($api, [
                        $date => 0
                    ]);
                }

                if (gettype($option) == 'array') {
                    if (array_key_exists('follow', $option)) {
                    }
                }

                return $result;
            } else {
                if (gettype($data) == 'integer') {
                    if ($cache->has($api)) {
                        $result = $cache->get($api);
                        if (array_key_exists($date, $result)) {
                            $result[$date] += 1;
                        } else {
                            $result[$date] = 1;
                        }
                        $cache->set($api, $result);
                    } else {
                        $cache->set($api, [
                            $date => 1
                        ]);
                    }
                }
            }
        } else {
            $date = date('Y-m-d');

            if ($data == 0) {

                // 完成访问操作
                if ($cache->hexists('admin_vstate', $api)) {
                    $result = json_decode($cache->hget('admin_vstate', $api), true);
                    if (array_key_exists($date, $result)) {
                        $result = $result[$date];
                    } else {
                        if (count($result) > 7) {
                            for ($i = 8; $i <= count($result); $i++) {
                                unset($result[date('Y-m-d', strtotime('-' . $i . ' days'))]);
                            }
                        }
                        $result[$date] = 0;
                        $cache->hset('admin_vstate', $api, json_encode($result, JSON_UNESCAPED_UNICODE));
                        $result = 0;
                    }
                } else {
                    $result = 0;
                    $cache->hset('admin_vstate', $api, json_encode([
                        $date => 0
                    ], JSON_UNESCAPED_UNICODE));
                }

                if (gettype($option) == 'array') {
                    if (array_key_exists('follow', $option)) {
                    }
                }

                return $result;
            } else {
                if (gettype($data) == 'integer') {
                    if ($cache->hexists('admin_vstate', $api)) {
                        $result = json_decode($cache->hget('admin_vstate', $api), true);
                        if (array_key_exists($date, $result)) {
                            $result[$date] += 1;
                        } else {
                            $result[$date] = 1;
                        }
                        $cache->hset('admin_vstate', $api, json_encode($result, JSON_UNESCAPED_UNICODE));
                    } else {
                        $cache->hset('admin_vstate', $api, json_encode([
                            $date => 1
                        ], JSON_UNESCAPED_UNICODE));
                    }
                }
            }
        }
    }

    // LyMaster 日志系统
    public static function logger()
    {
        return new ToolsLogger();
    }

    // LyMaster 当前是否 debug 模式
    public static function isDebug()
    {
        $debug_config = Config::getConfig('develop', 'admin', 'json');
        return $debug_config['framework-debug'];
    }
}
