<?php

namespace Plugin\LyMaster\core;

/**
 * Name: LyMaster.plugin.Processor
 * Author: LyAPI
 * ModifyTime: 2020/05/03
 * Purpose: 框架核心管理器
 */

class Master
{
    // 检查URL是否合法程序（用于插件URL验证）
    public static function urlFilter($url, $condition)
    {

        $paths = [];

        if(count($condition) == 1){
            $condition[1] = $condition[0];
            $condition[0] = "root";
        }

        foreach ($condition as $key => $value) {
            if ($value != '*') {
                if ($value != $url[$key]) {
                    return [false];
                }
            } else {
                if(count($url) > $key){
                    array_push($paths, $url[$key]);
                }
            }
        }

        // 检查长度是否符合
        if (count($url) != count($condition)) {
            return [false];
        }

        return [true, $paths];
    }
}
