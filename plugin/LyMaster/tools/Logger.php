<?php

namespace Plugin\LyMaster\tools;

use APP\program\admin\Setting;

/**
 * Name: LyMaster.tools.Logger
 * Author: LyAPI
 * ModifyTime: 2020/04/17
 * Purpose: LyApi Admin 日志系统
 */

class Logger
{
    // 写入新的数据日志
    public function write($type,$from,$context,$remark,$doip = null){
        if($doip == null){
            $doip = $_SERVER['REMOTE_ADDR'];
        }

        $db_connect = Setting::dbConnect();

        $db_connect->insert('logs',[
            'type' => $type,
            'from' => $from,
            'doip' => $doip,
            'info' => $context,
            'remark' => $remark
        ]);
    }
}