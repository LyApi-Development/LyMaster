<?php

namespace Plugin\LyMaster\tools;

use APP\program\admin\Setting;

/**
 * Name: LyMaster.tools.Logger
 * Author: LyAPI
 * ModifyTime: 2020/04/17
 * Purpose: LyApi Admin 日志系统
 */

class Database
{
    // 取得数据库配置
    public static function connects($target = 1)
    {
        $db_connect = Setting::dbConnect();
        $where = ['status' => 1];
        if (gettype($target) == 'integer') {
            $where['id'] = $target;
        } else if (gettype($target) == 'string') {
            $where['name'] = $target;
        } else {
            return [];
        }

        $data = $db_connect->get('database', '*', $where);
        if($data != NULL){
            return [
                'server' => $data['host'],
                'username' => $data['username'],
                'password' => $data['password'],
                'database_name' => $data['name'],
                'charset' => $data['charset'],
                'database_type' => $data['type']
            ];
        }else{
            return [];
        }
    }
}
