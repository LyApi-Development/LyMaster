<?php

namespace Plugin\OCore;

use APP\DI;
use APP\program\admin\Setting;
use APP\program\admin\Verify;
use Exception;
use LyApi\core\design\Register;
use LyApi\core\error\CustomException;
use LyApi\logger\Logger;
use LyApi\tools\Config;
use PDO;
use Plugin\Core\Core;
use Plugin\LyMaster\core\Master;
use Plugin\LyMaster\LyMaster;
use Plugin\PConfig\PConfig;

/**
 * Name: OCore.OCore
 * Author: LyAPI
 * ModifyTime: 2019/12/24
 * Purpose: OCore主程序
 */

class OCore extends Core
{

    private $Plugin_Config;

    //设置插件信息
    public function __construct()
    {
        $this->Plugin_Name = 'OCore';
        $this->Plugin_Version = 'V1.0.1';
        $this->Plugin_Author = 'mrxiaozhuox';
        $this->Plugin_About = '使用本插件接管框架ECore系统可获得更多功能（ 当前支持ECore for V1.6.7 ）';
        $this->Plugin_Examine = '';

        $this->Plugin_Config = new PConfig($this->Plugin_Name);

        // 插件配置文件
        $this->Plugin_Config->InitConfig("setting", array(
            "FilteWord" => "",
            "CacheMethod" => array(
                "Type" => "File",
                "Server" => array()
            )
        ));

        // 处理头文件
        $this->Plugin_Config->InitConfig("headers", array(
            "jpg" => "Content-type: image/jpg",
            "png" => "Content-type: image/jpg",
            "jpeg" => "Content-type: image/jpg",
            "js" => "Content-type: text/javascript",
            "css" => "Content-type: text/css",
            "json" => "Content-type: application/json",
            "zip" => "Content-Type: application/zip",
            "rar" => "Content-Type: application/zip",
            "pdf" => "Content-Type: application/pdf"
        ));

        // 文件路径映射 (项目迁移时需要更新或删除)
        $this->Plugin_Config->InitConfig("maps", array(
            "Resource" => LyApi . "/app/view/static/",
            "resource" => LyApi . "/app/view/static/",
        ));
    }

    // 接管函数：插件返回数据
    public function CreateResult($respnse_structure = [], $respnse_data = [], $respnse_order_data)
    {
        // 这个函数会在每次数据创建时被执行，主要用于最终数据的生成操作

        /**
         * @参数 respnse_structure: 请求的数据结构
         * @参数 respnse_data: 请求的数据内容
         * @参数 respnse_order_data: 请求的备用数据内容
         */

        // ---------- 这边给出官方操作以供参考 ---------- //

        // foreach ($respnse_structure as $key => $val) {
        //     if ($val == '$data') {
        //         $response[$key] = $respnse_data['data'];
        //     } else if ($val == '$code') {
        //         $response[$key] = $respnse_data['code'];
        //     } else if ($val == '$msg') {
        //         $response[$key] = $respnse_data['msg'];
        //     } else {
        //         if (array_key_exists($key, $respnse_data)) {
        //             $response[$key] = $respnse_data[$key];
        //         } elseif (array_key_exists(substr($val, 1), $respnse_order_data)) {
        //             $response[$key] = $respnse_order_data[substr($val, 1)];
        //         } else {
        //             $response[$key] = $val;
        //         }
        //     }
        // }

        // ---------- 这边给出官方操作以供参考 ---------- //


        // 如果继续使用内部生成系统，返回NULL即可
        return null;
    }

    // 接管函数：替换使用函数
    public function TargetFinding($using_namespace = '', $using_function = '')
    {

        // 这个函数会在调用函数使用前执行，你可以替换使用的函数

        // ---------- 这边给出Demo以供参考 ---------- //

        // PS: 当你不需要本功能，删除下方所有程序，提升接口运行效率

        // 获取 OCore 配置信息
        $setting = $this->Plugin_Config->ReadConfig('setting');

        // Demo首先对URL进行了简单对解析
        $all_path = explode('\\', $using_namespace . '\\' . $using_function);
        $first_path = $all_path[2];
        $maps = $this->Plugin_Config->ReadConfig('maps');

        // 判断当前访问对是不是静态资源页面
        // var_dump($first_path,$using_function,$all_path);
        if ((array_key_exists($using_function, $maps) && $first_path == '') ||
            array_key_exists($first_path, $maps)
        ) {

            if (array_key_exists($using_function, $maps)) {
                $nowmap = $maps[$using_function];
            } else {
                $nowmap = $maps[$first_path];
            }

            // 返回更新后使用的对象与函数
            return [
                'namespace' => 'APP\api\root',                      // 转到的命名空间（包括对象）
                'function' => 'index',                              // 转到的函数名
                'backval' => [                                      // 将数据传入处理函数，做第三个参数
                    'map' => $nowmap                                // 当前使用的资源目录
                ],
                'rewrite' => function ($type, $req, $backval) {         // 重写需要运行的函数

                    // 获取需要访问的文件
                    // var_dump($backval);
                    $uri = $_SERVER['REQUEST_URI'];

                    if (strrpos($uri, "?") != false) {
                        $uri = substr($uri, 0, strrpos($uri, "?"));
                    }

                    $path_list = array_filter(explode('/', $uri));
                    array_shift($path_list);

                    $file_path = implode('/', $path_list);

                    $config = new PConfig("OCore");
                    $setting = $config->ReadConfig("setting");

                    if (is_file($backval['map'] . $file_path)) {

                        $file = file_get_contents($backval['map'] . $file_path);

                        $suffix = pathinfo($file_path, PATHINFO_EXTENSION);
                        $headers = $this->Plugin_Config->ReadConfig("headers");

                        // 对一些特殊文件进行处理
                        if (array_key_exists($suffix, $headers)) {
                            header($headers[$suffix]);
                        }

                        return $file;
                    } else {
                        throw new CustomException('Invalid Request: Resource file not found');
                    }
                }
            ];
        } else {

            // 过滤器检查
            $filters = Config::getConfig('develop', 'admin', 'json')['vstate-setting']['filter'];

            $filter_pass = true;

            foreach ($filters as $key => $filter) {
                if (array_key_exists('using-function', $filter) && array_key_exists('first-path', $filter)) {
                    if ($using_function == $filter['using-function'] && $first_path == $filter['first-path']) {
                        $filter_pass = false;
                        break;
                    }
                } else {
                    if (array_key_exists('using-function', $filter)) {
                        if ($using_function == $filter['using-funciton']) {
                            $filter_pass = false;
                            break;
                        }
                    } else if (array_key_exists('first-path', $filter)) {
                        if ($first_path == $filter['first-path']) {
                            $filter_pass = false;
                            break;
                        }
                    }
                }
            }

            if ($filter_pass) {
                $admin = new LyMaster();
                $admin->visitState('all', 1);
                $admin->visitState($using_namespace, 1);
            }

            // if (!($using_function == 'admin' && $first_path == 'root' ||
            //     $first_path == 'admin' ||
            //     $using_function == 'favicon.ico' && $first_path == 'root')) {

            //     // $logger = new Logger();
            //     // $logger->SetLogger([
            //     //     'time' => date('Y-m-d'),
            //     //     'typs' => 'Test',
            //     //     'message' => $using_namespace . ' - ' . $using_function . ': 新的访问已写入！'
            //     // ]);

            //     // LyApi Admin 接口访问统计程序
            //     $admin = new LyMaster();
            //     $admin->visitState('all', 1);
            //     $admin->visitState($using_namespace, 1);
            // }
        }

        // ---------- 这边给出Demo以供参考 ---------- //

        // LyApi - Admin (类型判断并自动更新头文件)
        if (is_subclass_of($using_namespace, 'LyApi\core\classify\API')) {
            header('Content-type: application/json');
        }

        if (Setting::isInstall()) {
            // LyApi - Admin 插件加载
            $pmanager = Register::Get('PManager');
            $pmanager->registerAll();
            $pmanager = Register::Set('PManager', $pmanager);

            // LyMaster 插件接口注册
            $pmanager = Register::Get('PManager');
            $papis = $pmanager->pluginApis();

            if ($papis == '') {
                $papis = [];
            }

            foreach ($papis as $key => $value) {
                $paths = $value['options']['path'];
                $paths = explode('/', $paths);
                $filter_r = Master::urlFilter(array_slice($all_path, 2), $paths);
                if ($filter_r[0]) {
                    $value['backval']['holder'] = $filter_r[1];
                    return [
                        'namespace' => $value['options']['_namspace'],
                        'function' => $value['options']['_function'],
                        'backval' => $value['backval'],
                        'rewrite' => $value['function']
                    ];
                }
                // 测试用途，请勿开启
                //  else {
                //     return [
                //         'namespace' => 'APP\api\root',
                //         'function' => 'admin',
                //         'backval' => [
                //             $filter_r
                //         ],
                //         'rewrite' => function($type, $args, $backval){
                //             return json_encode($backval);
                //         }
                //     ];
                // }
            }
        }


        // 没有被特殊处理就正常运行
        return [
            'namespace' => $using_namespace,
            'function' => $using_function
        ];
    }

    // 接管函数：处理最终输出
    public function FinalExamine($respnse_structure = [], $respnse_data = [])
    {
        // 这个函数会在数据正常输出前把结构和内容交给你做最后一次检查，你可以在这里进行处理。

        // $respnse_structure = [];

        // var_dump($respnse_data);

        // 进行关键词过滤（不需要注释，提高程序速度）
        // $config = new PConfig('OCore');
        // $words = explode(',', $config->ReadConfig("setting")['FilteWord']);
        // foreach ($respnse_data as $key => $val) {
        //     foreach ($words as $word) {
        //         $val = str_ireplace($word, '**', $val);
        //     }
        //     $respnse_data[$key] = $val;
        // }


        return [
            'structure' => $respnse_structure,
            'data' => $respnse_data
        ];
    }

    // 接管函数：插件初始化
    public function InitPlugin($plugin_name = '', $plugin_version = '')
    {
        // 这个函数会在所有插件被初始化时调用，你可以在这里进行前置操作

        // 返回的数据将存入对象的 Tmp_Data 数据下
        return [];
    }
}
