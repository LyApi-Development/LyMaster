<?php

namespace APP\api\admin\api;

use APP\DI;
use APP\program\admin\Apis;
use APP\program\admin\Cache;
use APP\program\admin\Setting;
use APP\program\admin\Verify;
use LyApi\core\classify\API;
use LyApi\core\error\CustomException;
use LyApi\core\request\Request;
use LyApi\tools\Config;

// 一些涉及系统安全的高级接口
class summit extends API
{
    // 取得项目所有文件内容
    public function folders($type, $args)
    {

        // 一级验证（来源验证）
        if (array_key_exists('from', $args)) {
            if ($args['from'] != 'coder-administator') {
                return ['#code' => '401', '#msg' => '账号无权限'];
            }
        }

        // 验证用户信息
        if (!Verify::isLogin()) {
            return ['#code' => '401', '#msg' => '账号未登录'];
        } else {
            if (!Verify::authCheck('docoder')) {
                return ['#code' => '401', '#msg' => '账号无权限'];
            }
        }

        $filter = Config::getConfig('summit', 'admin', 'json')['coder']['overview_filter'];
        $filter = array_merge($filter, Config::getConfig('coder', 'admin', 'json')['overview_filter']);

        $root_path = Config::getConfig('summit', 'admin', 'json')['system']['root_path'];
        if(! is_dir($root_path)){
            $root_path = LyApi;
        }

        // 验证数据是否缓存
        $cahce = DI::FileCache('admin/coder');

        if($cahce->has('froot')){
            return $cahce->get('froot');
        }else{
            $ctime = (int)Cache::cacheTime();
            $trees = Apis::allTree($root_path, $filter);
            if($ctime > 0){
                $cahce->set('froot',$trees,$ctime);
            }
            return $trees;

        }
    }
}
