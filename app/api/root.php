<?php

/**
 * 本Class默认渲染主页面，可自行更改。
 * LyApi - Admin
 */

namespace APP\api;

use APP\DI;
use APP\program\admin\Setting;
use APP\program\admin\user\Database;
use APP\program\admin\Verify;
use APP\program\Resource;
use LyApi\core\classify\VIEW;
use LyApi\tools\Template;

class root extends VIEW
{

    private $resources;

    // 初始化函数
    public function __construct()
    {
        $this->resources = new Resource();
    }

    // 主页面渲染：处理程序
    public function index()
    {
        // 检测是否安装完毕
        if (Setting::isInstall()) {

            $cute = Setting::localDB('plugin');
            $idx = $cute->get('index_control');
            if ($idx != false) {
                $namespace = "Admin\\plugin\\" . $idx . '\\options';
                if (class_exists($namespace)) {

                    $db_connect = Setting::dbConnect();
                    if ($db_connect->has('plugin', ['status' => '1', 'sign' => $idx])) {
                        $plugin = new $namespace();
                        return $plugin->indexPage();
                    }
                }
            }

            return Template::RenderTemplate(file_get_contents(LyApi . '/app/view/html/index.html'), [
                'layJs' => $this->resources->LayUI('js'),
                'layCss' => $this->resources->LayUI('css'),
                'layExtend' => $this->resources->LayUI('extend'),

                'siteNotice' => Setting::getSetting('site_notice'),
                'siteName' => Setting::getSetting('site_title')
            ]);
        } else {
            // 自动跳转至安装页面
            return '<script>location.href="/install"</script>';
        }
    }

    // 安装页面渲染
    public function install()
    {
        if (!Setting::isInstall()) {
            return Template::RenderTemplate(
                file_get_contents(LyApi . '/app/view/html/install.html'),
                [
                    'LayJs' => $this->resources->LayUI('js'),
                    'LayCss' => $this->resources->LayUI('css'),
                    'StepJs' => $this->resources->LayUI('extend') . 'step/',
                    'StepCss' => $this->resources->LayUI('extend') . 'step/step.css',

                    'SDomain' => $this->resources->Domain()
                ]
            );
        } else {
            return Template::RenderTemplate(file_get_contents(LyApi . '/app/view/error/404.html'));
        }
    }

    // 后台主页渲染程序 (切勿修改，否则无法访问后台！！)
    public function admin()
    {
        if (Verify::isLogin()) {

            $notice = null;

            $status = Database::initDB($_SESSION['userid']);
            if (!$status) {
                $notice = '用户数据目录初始化失败...';
            }

            return Template::RenderTemplate(file_get_contents(LyApi . '/app/view/html/admin/index.html'), [
                'resource' => $this->resources->Domain() . '/resource/admin',
                'nickName' => $_SESSION['nickname'],
                'initNotice' => $notice
            ]);
        } else {
            return '<script>location.href="/admin/verify"</script>';
        }
    }
}
