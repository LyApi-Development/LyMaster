<?php

namespace APP\program\system;

// 系统操作程序
class System
{
    public static function deldir($dir)
    {
        if (file_exists($dir)) {
            $files = scandir($dir);
            foreach ($files as $file) {
                if ($file != '.' && $file != '..') {
                    $path = $dir . '/' . $file;
                    if (is_dir($path)) {
                        self::deldir($path);
                    } else {
                        unlink($path);
                    }
                }
            }
            rmdir($dir);
            return true;
        } else {
            return false;
        }
    }

    public static function getRandomStr($len, $special = true)
    {
        // $chars = array(
        //     "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
        //     "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
        //     "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
        //     "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        //     "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
        //     "3", "4", "5", "6", "7", "8", "9"
        // );

        $chars = array(
            "A", "B", "C", "D", "E", "F", "G",
            "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
            "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
            "3", "4", "5", "6", "7", "8", "9"
        );

        if ($special) {
            $chars = array_merge($chars, array(
                "!", "@", "#", "$", "?", "|", "{", "/", ":", ";",
                "%", "^", "&", "*", "(", ")", "-", "_", "[", "]",
                "}", "<", ">", "~", "+", "=", ",", "."
            ));
        }

        $charsLen = count($chars) - 1;
        shuffle($chars);                            //打乱数组顺序
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, $charsLen)];    //随机取出一位
        }
        return $str;
    }
}
