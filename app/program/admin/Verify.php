<?php

namespace APP\program\admin;

use APP\DI;
use LyApi\tools\Config;
use Medoo\Medoo;

/**
 * LyApi - Admin System
 * 用于信息验证
 */

class Verify
{

    // 用户是否登录
    public static function isLogin()
    {

        if (!array_key_exists('username', $_SESSION)) {
            return false;
        }

        if ($_SESSION['username'] != null && $_SESSION['userid'] != null) {
            
            $db_connect = Setting::dbConnect();

            if (! $db_connect->has('users',['id' => $_SESSION['userid']])) {
                unset($_SESSION['userid']);
                unset($_SESSION['username']);
                unset($_SESSION['nickname']);
                return false;
            }
            
            return true;
        }

        return false;
    }

    // 用户密码检查
    public static function encryptPwd($password, $secret)
    {
        return md5(md5($password) . $secret);
    }

    // 权限判定程序
    public static function authCheck($authority)
    {
        // 判定用户是否登录
        if (self::isLogin()) {
            $db_connect = Setting::dbConnect();
            if ($db_connect->has('users', ['username' => $_SESSION['username']])) {
                $level = $db_connect->get('users', 'level', ['username' => $_SESSION['username']]);
                if ($db_connect->has('user_level', ['id' => $level, $authority => '1'])) {
                    return true;
                }
            }
        }
        return false;
    }
}
