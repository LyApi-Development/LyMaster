<?php

namespace APP\program\admin;

use APP\DI;
use LyApi\tools\Config;
use Medoo\Medoo;

/**
 * LyApi - Admin System
 * 用于插件系统操作
 */

class Plugin
{
    public function reloadPlugin()
    {
        $rpath = LyApi . '/admin/plugin/';
        $plugins = scandir($rpath);
        $success = 0;

        $db_connect = Setting::dbConnect();

        foreach ($plugins as $key => $value) {
            if ($value == '.' || $value == '..') {
                continue;
            }

            if (is_dir($rpath . $value)) {
                if (!$db_connect->has('plugin', ['path' => $value])) {
                    $mnsp = "Admin\\plugin\\" . $value;
                    if (class_exists($mnsp . "\\options")) {
                        $class = $mnsp . "\\options";
                        $pcl = new $class;

                        $plugin_sign = $pcl::SIGN;
                        $plugin_name = $pcl::NAME;
                        $plugin_version = $pcl::VERSION;

                        if (property_exists($pcl, 'tables')) {
                            $plugin_tables = $pcl->tables;
                        } else {
                            $plugin_tables = [];
                        }

                        if (property_exists($pcl, 'config')) {
                            $plugin_config = $pcl->config;
                        } else {
                            $plugin_config = [];
                        }

                        if(method_exists($pcl,'onInstall')){
                            $pcl->onInstall();
                        }

                        if($pcl::USABLE){
                            $inst = $db_connect->insert('plugin', [
                                'sign' => $plugin_sign,
                                'name' => $plugin_name,
                                'version' => $plugin_version,
                                'database' => $plugin_tables,
                                'config' => $plugin_config,
                                'path' => $value
                            ]);
                            if ($inst->errorCode() == '00000') {
                                $success++;
                            }
                        }
                    }
                }
            }
        }
        return $success;
    }

    public function reloadSet($id)
    {
        $rpath = LyApi . '/admin/plugin/';

        $db_connect = Setting::dbConnect();

        if (!$db_connect->has('plugin', ['id' => $id])) {
            return  false;
        }

        $path = $db_connect->get('plugin', 'path', ['id' => $id]);

        if (is_dir($rpath . $path)) {
            $mnsp = "Admin\\plugin\\" . $path;
            if (class_exists($mnsp . "\\options")) {
                $class = $mnsp . "\\options";
                $pcl = new $class;

                $plugin_sign = $pcl::SIGN;
                $plugin_name = $pcl::NAME;
                $plugin_version = $pcl::VERSION;

                if (property_exists($pcl, 'tables')) {
                    $plugin_tables = $pcl->tables;
                } else {
                    $plugin_tables = [];
                }

                if (property_exists($pcl, 'config')) {
                    $plugin_config = $pcl->config;
                } else {
                    $plugin_config = [];
                }

                $inst = $db_connect->update('plugin', [
                    'sign' => $plugin_sign,
                    'name' => $plugin_name,
                    'version' => $plugin_version,
                    'database' => $plugin_tables,
                    'config' => $plugin_config,
                    'path' => $path
                ],['id' => $id]);
                if ($inst->errorCode() == '00000') {
                    return true;
                }
            }
        }

        return false;
    }

    // 检查插件是否开启
    public static function isOpen($data,$type = 'name'){
        $db_connect = Setting::dbConnect();
        $status = $db_connect->get('plugin','status',[$type => $data]);
        if($status == '1'){
            return true;
        }
        return false;
    }

}
