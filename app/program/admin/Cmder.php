<?php

namespace APP\program\admin;

use APP\DI;
use APP\program\admin\Verify;
use LyApi\core\design\Register;
use Plugin\LyMaster\plugin\Manager;

/**
 * LyApi - Admin System
 * 用于 LyMaster Cmder 操作
 */

class Cmder
{

    private $authority = "docoder";

    public function explain($cmd)
    {
        if (!Verify::authCheck($this->authority)) {
            return "当前账号无使用权限！";
        }

        $commands = explode(" ", $cmd);

        if (count($commands) > 0) {
            $commands[0] = strtolower($commands[0]);
            if ($commands[0] == 'whoami') {
                $type = "nickname";
                if (count($commands) > 1) {
                    if ($commands[1] == "-nick" || $commands[1] == '-n' || $commands == '-N') {
                        $type = "nickname";
                    } else if ($commands[1] == "-user" || $commands[1] == '-u' || $commands == '-U') {
                        $type = "username";
                    }
                }
                return $_SESSION[$type];
            } else if ($commands[0] == 'sysinfo') {
                return "<script>layer.open({
                    type: 2,
                    title: 'System Info - 系统信息',
                    shadeClose: true,
                    shade: false,
                    maxmin: false,
                    area: ['420px', '360px'],
                    content: '/admin/coder?child=sysinfo'
                });</script>";
            } else if ($commands[0] == 'maker') {
                // Maker Command 程序
                $res = $this->maker($commands);
                if ($res != '' && $res != null) {
                    return $res;
                }
            } else if ($commands[0] == 'cache') {
                if (count($commands) > 1 && strtolower($commands[1]) == 'clean') {
                    Cache::cleanFileCahce();
                    return "后台缓存清理成功！";
                } else {
                    return "使用 cache clean 可快速清除缓存数据";
                }
            } else {
                $res = $this->plugin_cmd($commands);
                if ($res != '' && $res != null) {
                    return $res;
                }
            }
        }

        return "未知的程序指令：" . $cmd;
    }


    // 插件命令处理器
    public function plugin_cmd($commands)
    {
        $pmanager = Register::Get('PManager');
        $data = $pmanager->pluginCmds();
        foreach ($data as $key => $value) {
            if (in_array($commands[0], $value['keywords'])) {
                $func = $value['function'];
                return $func($commands, count($commands));
            }
        }
    }

    // Maker Tool 处理程序
    private function maker($commands)
    {
        if (count($commands) > 1) {
            if ($commands[1] == 'icd' || $commands[1] == 'include') {
                if (count($commands) > 2) {
                    if ($commands[2] == 'add') {
                        if (count($commands) > 3) {
                            $cache = DI::FileCache('admin/maker');
                            $used = $cache->get('used-list');

                            $commands[3] = str_replace('.', '\\', $commands[3]);

                            if (!in_array($commands[3], $used)) {
                                array_push($used, $commands[3]);
                            }
                            $cache->set('used-list', $used);
                            return "代码生成器引入空间成功：" . $commands[3];
                        } else {
                            return "插入格式: maker icd add foo\\bar";
                        }
                    } else if ($commands[2] == 'del') {
                        if (count($commands) > 3) {
                            $cache = DI::FileCache('admin/maker');
                            $used = $cache->get('used-list');

                            $commands[3] = str_replace('.', '\\', $commands[3]);

                            if (in_array($commands[3], $used)) {
                                unset($used[(int) array_search($commands[3], $used)]);
                            }

                            $cache->set('used-list', $used);
                            return "代码生成器删除空间成功：" . $commands[3];
                        } else {
                            return "删除格式: maker icd del foo\\bar";
                        }
                    } else if ($commands[2] == 'list') {
                        $cache = DI::FileCache('admin/maker');
                        $used = $cache->get('used-list');
                        return json_encode(array_filter($used));
                    } else if ($commands[2] == 'clean') {
                        $cache = DI::FileCache('admin/maker');
                        $cache->set('used-list', []);
                        return "已成功清空所有引用！";
                    }
                } else {
                    return "[引用] 操作列表: add(新增引用), del(删除引用), clean(清空引用) list(引用列表)";
                }
            } else if ($commands[1] == 'func' || $commands[1] == 'function') {
                if (count($commands) > 2) {
                    if ($commands[2] == 'list') {
                        $cache = DI::FileCache('admin/maker');
                        $comps = $cache->get('comp-list');
                        $res = "<ul>";
                        foreach ($comps as $key => $value) {
                            $res .= '<li>' . $value['func'] . " [" . $value['name'] . ']</li>';
                        }
                        return $res . '</ul>';
                    }
                } else {
                    return "[函数] 操作列表: list(函数列表)";
                }
            } else if ($commands[1] == 'help') {
                return "<script>window.open('http://lyapi-development.gitee.io/lymaster/#/cmder/maker')</script>";
            } else {
                return "未知的操作请求: " . $commands[1];
            }
        } else {
            return "「Maker Cmder」用于实时管理代码生成器数据...";
        }
    }
}
