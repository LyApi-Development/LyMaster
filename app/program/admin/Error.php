<?php

namespace APP\program\admin;

use APP\DI;
use APP\program\Resource;
use APP\program\system\System;
use Exception;
use LyApi\tools\Config;
use LyApi\tools\Template;
use Predis\Connection\ConnectionException;

/**
 * LyApi - Admin System
 * 用于Admin异常页面渲染
 */

class Error
{
    // 渲染 运行时异常
    public static function runtimeError($options = [])
    {
        return Template::RenderTemplate(
            file_get_contents(LyApi . '/app/view/error/runtime.html'),
            $options
        );
    }

    public static function _404Error()
    {
        $resources = new Resource();
        return Template::RenderTemplate(
            file_get_contents(LyApi . '/app/view/html/admin/page/404.html'),
            [
                'domain' => $resources->Domain()
            ]
        );
    }
}
