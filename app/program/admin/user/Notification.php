<?php

namespace APP\program\admin\user;

use APP\DI;
use LyApi\tools\Config;
use Medoo\Medoo;

/**
 * LyApi - Admin System
 * 用户.通知操作程序
 */

class Notification
{
    // 发送消息函数
    public static function sendMessage($context, $from = '系统官方', $type = 'info', $preview = null, $to = null)
    {
        $db = Database::openDB('message', $to);

        if (
            $type != 'info' && $type != 'warning' &&
            $type != 'error' && $type == 'success'
        ) {
            $type = 'info';
        }

        if ($preview == null) {
            $preview = '打开消息页面方可查看详情！';
        }

        if ($db != null) {
            $list = $db->get('message');
            $indexs = $db->get('indexs');

            if (!$list) {
                $list  = [];
            } else {
                $list = json_decode($list, true);
            }

            if (!$indexs) {
                $indexs = 0;
            }

            array_push($list, [
                'id' => ($indexs + 1),
                'context' => $context,
                'preview' => $preview,
                'type' => $type,
                'readed' => false,
                'from' => $from,
                'date' => date('Y-m-d')
            ]);

            $db->set('indexs', $indexs + 1, true);

            return $db->set('message', json_encode($list), true);
        }
    }

    public static function messageList()
    {
        $db = Database::openDB('message');
        $list = $db->get('message');
        return json_decode($list, true);
    }

    public static function getMesssage($id)
    {
        $db = Database::openDB('message');
        $list = $db->get('message');
        if ($list != false) {
            $list = json_decode($list, true);
            foreach ($list as $key => $value) {
                if ($value['id'] == $id) {
                    return $value;
                }
            }
        }
        return null;
    }

    // 接收新消息 (仅未查看的消息)
    public static function newMessage($clean = false, $num = 0)
    {
        $db = Database::openDB('message');
        $list = $db->get('message');
        if (!$list) {
            return '';
        }

        $list = json_decode($list, true);

        $res = [];
        foreach ($list as $key => $value) {

            if (count($res) >= $num && $num != 0) {
                break;
            }

            if (!$value['readed']) {
                array_push($res, $value);
                if ($clean) {
                    $list[$key]['readed'] = true;
                }
            }
        }

        if ($clean) {
            $db->set('message', json_encode($list), true);
        }

        return $res;
    }


    public static function cleanAll()
    {
        $db = Database::openDB('message');
        $db->delete('message');
        $db->set('indexs', 0, true);
    }

    public static function readit($id)
    {
        $db = Database::openDB('message');
        $msg = $db->get('message');
        $msg = json_decode($msg, true);
        foreach ($msg as $key => $value) {
            if ($value['id'] == $id) {
                $msg[$key]['readed'] = true;
            }
        }
        $db->set('message', json_encode($msg), true);
    }

    public static function delete($id)
    {
        $db = Database::openDB('message');
        $msg = $db->get('message');
        $msg = json_decode($msg, true);
        foreach ($msg as $key => $value) {
            if ($value['id'] == $id) {
                array_splice($msg, $key, 1);
            }
        }
        $db->set('message', json_encode($msg), true);
    }
}
