<?php

namespace APP\program\admin;

use APP\DI;
use APP\program\CuteDB;
use LyApi\tools\Config;
use Medoo\Medoo;

/**
 * LyApi - Admin System
 * 用于系统配置读取
 */

class Setting
{

    // 判断数据库是否已连接
    public static function isInstall()
    {
        return file_exists(LyApi . '/config/admin/database.json');
    }

    public static function dbConnect()
    {
        if (self::isInstall()) {
            if (file_exists(LyApi . '/config/admin/database.json')) {
                return new Medoo(Config::getConfig('database', 'admin', 'json'));
            }
        }
        return null;
    }

    // 取得系统配置
    public static function getSetting($comment)
    {
        $db_connect = self::dbConnect();
        if ($db_connect != null) {
            return $db_connect->get('setting', 'context', ['comment' => $comment]);
        } else {
            return '';
        }
    }

    // 启动local CuteDB
    public static function localDB($dbname)
    {
        $db = new CuteDB();
        $db->open(LyApi . '/data/database/local/' . $dbname);
        return $db;
    }
}
