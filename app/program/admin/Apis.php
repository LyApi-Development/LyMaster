<?php

namespace APP\program\admin;

use APP\DI;
use LyApi\tools\Config;
use Medoo\Medoo;

/**
 * LyApi - Admin System
 * 用于接口操作
 */

class Apis
{
    public static function apiTree()
    {
        $db_connect = Setting::dbConnect();

        $data = $db_connect->select('apis', ['apipath', 'apiname']);

        foreach ($data as $key => $value) {
            $data[$key]['apipath'] = $data[$key]['apipath'] . '.php';
        }

        return $data;
    }

    public static function allTree($dir, $filter = [])
    {
        //定义一个数组
        $files = array();
        //检测是否存在文件
        if (is_dir($dir)) {
            //打开目录
            if ($handle = opendir($dir)) {
                //返回当前文件的条目
                while (($file = readdir($handle)) !== false) {
                    //去除特殊目录
                    if ($file != "." && $file != "..") {

                        if (in_array($file, $filter)) {
                            continue;
                        }

                        //判断子目录是否还存在子目录
                        if (is_dir($dir . "/" . $file)) {
                            //递归调用本函数，再次获取目录
                            array_push($files, array(
                                'title' => $file,
                                'children' => self::allTree($dir . "/" . $file, $filter)
                            ));
                        } else {
                            //获取目录数组
                            $files[] = array(
                                'title' => $file,
                                'path' => $dir
                            );
                        }
                    }
                }
                //关闭文件夹
                closedir($handle);
                //返回文件夹数组
                return $files;
            }
        }
    }
}
