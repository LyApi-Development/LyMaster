<?php

namespace APP\program;

use LyApi\tools\Config;

/**
 * LyApi - Admin System
 * 用于生成静态文件路径
 */

class Resource
{

    // 配置文件加载
    private $developConfig;
    private $sourceDomain;

    // 构造函数，自动读取需要的文件
    public function __construct()
    {
        $this->developConfig = Config::getConfig('develop', 'admin', 'json');
        $httpType = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';

        if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1') {
            $this->sourceDomain = $httpType .
                $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'];
        } else {

            if($_SERVER['SERVER_PORT'] != "80"){
                $this->sourceDomain = $httpType . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'];
            }else{
                $this->sourceDomain = $httpType . $_SERVER['SERVER_NAME'];
            }

        }
    }

    // 取得当前系统访问域名 
    public function Domain()
    {
        return $this->sourceDomain;
    }

    // 取得 Layui 资源文件
    public function LayUI($type = 'js')
    {
        if ($type == 'js') {
            return $this->sourceDomain . '/resource/admin/layui/layui.js';
        } elseif ($type == 'css') {
            return $this->sourceDomain . '/resource/admin/layui/css/layui.css';
        } else {
            return $this->sourceDomain . '/resource/admin/layui/extend/';
        }
    }
}
